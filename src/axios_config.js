import axios from 'axios'
//import storage from '@/configuration/Storage.js'

const urlBase = 'https://pruebas-muy-candidatos.s3.us-east-2.amazonaws.com/RH.json'

const apiService = axios.create({
  baseURL: urlBase,
  headers: {'Content-Type': 'application/json'}
});

export const getUsersEndpoint = async () => {
	try{
		const result = await apiService('', {
			method: 'GET'
		})

		return result.data.response.users
	} catch (error) {
		return error.response
	}
}

export const getPizzeriasEndpoint = async () => {
	try{
		const result = await apiService('', {
			method: 'GET'
		})

		return result.data.response.stores
	} catch (error) {
		return error.response
	}
}