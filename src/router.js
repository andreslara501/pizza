import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'start',
      component: () => import('@/views/Login')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/views/Home')
    }
  ],
});

export default router;