import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex);

import { getUsersEndpoint, getPizzeriasEndpoint } from '@/axios_config'

const state = {
    users: [],
    pizzerias: [],
    logged: false,
}

const getters = {
    allUsers: (state) => state.users,
    logged: (state) => {

        return state.logged
    },
    allPizzerias: (state) => state.pizzerias,
}

const actions = {
    async getUsers({ commit }) {
        const users = await getUsersEndpoint()
        commit('SET_USERS', users)
    },
    async getPizzerias({ commit }) {
        const pizzerias = await getPizzeriasEndpoint()
        commit('SET_PIZZERIAS', pizzerias)
    },
}

const mutations = {
    SET_USERS(state, users) {
        state.users = users
    },
    LOGGED(state){
        state.logged = true
        localStorage.setItem('logged', 'true')
    },
    LOGOUT(state){
        state.logged = false
        localStorage.setItem('logged', 'false')
    },
    SET_PIZZERIAS(state, pizzerias){
        state.pizzerias = pizzerias
    },
}

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
})